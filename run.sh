#!/bin/sh

CLASSPATH="bin:lib/jersey-bundle-1.19.1.jar:lib/json-simple-1.1.1.jar" 

case "$1" in 
  "fsys")
     java -Dfile.encoding=UTF-8 -classpath $CLASSPATH fsys.FsysServerPublisher
     ;;
  "intl")
     java -Dfile.encoding=UTF-8 -classpath $CLASSPATH intl.IntlRESTStartUp
     ;; 
  "intlclient")
     java -Dfile.encoding=UTF-8 -classpath $CLASSPATH intlclient.IntlClient
     ;;
  "wsimport")
     cd src
     wsimport -keep http://localhost:8005/fsys?wsdl
     ;;
   *) 
     echo "Usage: startup fsys | intl | intlclient | wsimport"; 
     ;;
esac
