package fsys;

import java.util.*;

// A super-simple Customer DB just for illustration purposes.  
public class FsysDB {
	private Hashtable<String, Integer> customerHT   = new Hashtable();
	private Hashtable<Integer, Integer> insuranceHT = new Hashtable();
	private Hashtable<Integer, String> statusHT     = new Hashtable();
	
	private Integer customerId  = 1000;
	private Integer agreementId = 5000;
	
	// For simplicity we only store the SSN. One customerID per SSN. 
	public Integer addCustomer(String SSN, String firstName, String lastName) {
		if(customerHT.containsKey(SSN) == false) {
			customerHT.put(SSN,  ++this.customerId);
		}
		return customerHT.get(SSN);
	}
	
	// And we only have one contract per customer-ID -> agreementId.
	// Returns agreementId. 
	public Integer addInsuranceContract(Integer customerId) {
		if(insuranceHT.containsKey(customerId) == false) {
			insuranceHT.put(customerId, ++this.agreementId);
		}
		return insuranceHT.get(customerId);
	}
	
	// And one status. 
	public String setStatus(Integer sessionId, String status) {
		if(statusHT.containsKey(sessionId) == false) {
			statusHT.put(sessionId, status);
		}
		return statusHT.get(sessionId);
	}
	
	public String getStatus(Integer sessionId) {
		return statusHT.get(sessionId);
	}
}
