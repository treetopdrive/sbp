// The startup function. 

package fsys;

import javax.xml.ws.Endpoint; 

import java.io.IOException;

public class FsysServerPublisher {
	public static void main(String args[]) throws IOException {
		Endpoint server = Endpoint.create(new FsysServerImpl());
		server.publish("http://127.0.0.1:8005/fsys");
		
		System.out.println("Press Enter to stop the server. ");
		System.in.read();
		server.stop();
	}
}
