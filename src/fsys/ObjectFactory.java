
package fsys;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fsys package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendLetterResponse_QNAME = new QName("http://fsys/", "sendLetterResponse");
    private final static QName _CreateInsuranceAgreement_QNAME = new QName("http://fsys/", "createInsuranceAgreement");
    private final static QName _GetStatusResponse_QNAME = new QName("http://fsys/", "getStatusResponse");
    private final static QName _CreateCustomerResponse_QNAME = new QName("http://fsys/", "createCustomerResponse");
    private final static QName _CreateCustomer_QNAME = new QName("http://fsys/", "createCustomer");
    private final static QName _CreateInsuranceAgreementResponse_QNAME = new QName("http://fsys/", "createInsuranceAgreementResponse");
    private final static QName _SendLetter_QNAME = new QName("http://fsys/", "sendLetter");
    private final static QName _SetStatusResponse_QNAME = new QName("http://fsys/", "setStatusResponse");
    private final static QName _SetStatus_QNAME = new QName("http://fsys/", "setStatus");
    private final static QName _GetStatus_QNAME = new QName("http://fsys/", "getStatus");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fsys
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link SendLetterResponse }
     * 
     */
    public SendLetterResponse createSendLetterResponse() {
        return new SendLetterResponse();
    }

    /**
     * Create an instance of {@link CreateInsuranceAgreement }
     * 
     */
    public CreateInsuranceAgreement createCreateInsuranceAgreement() {
        return new CreateInsuranceAgreement();
    }

    /**
     * Create an instance of {@link CreateCustomer }
     * 
     */
    public CreateCustomer createCreateCustomer() {
        return new CreateCustomer();
    }

    /**
     * Create an instance of {@link CreateInsuranceAgreementResponse }
     * 
     */
    public CreateInsuranceAgreementResponse createCreateInsuranceAgreementResponse() {
        return new CreateInsuranceAgreementResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse }
     * 
     */
    public CreateCustomerResponse createCreateCustomerResponse() {
        return new CreateCustomerResponse();
    }

    /**
     * Create an instance of {@link SendLetter }
     * 
     */
    public SendLetter createSendLetter() {
        return new SendLetter();
    }

    /**
     * Create an instance of {@link SetStatusResponse }
     * 
     */
    public SetStatusResponse createSetStatusResponse() {
        return new SetStatusResponse();
    }

    /**
     * Create an instance of {@link GetStatus }
     * 
     */
    public GetStatus createGetStatus() {
        return new GetStatus();
    }

    /**
     * Create an instance of {@link SetStatus }
     * 
     */
    public SetStatus createSetStatus() {
        return new SetStatus();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendLetterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "sendLetterResponse")
    public JAXBElement<SendLetterResponse> createSendLetterResponse(SendLetterResponse value) {
        return new JAXBElement<SendLetterResponse>(_SendLetterResponse_QNAME, SendLetterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateInsuranceAgreement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "createInsuranceAgreement")
    public JAXBElement<CreateInsuranceAgreement> createCreateInsuranceAgreement(CreateInsuranceAgreement value) {
        return new JAXBElement<CreateInsuranceAgreement>(_CreateInsuranceAgreement_QNAME, CreateInsuranceAgreement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "getStatusResponse")
    public JAXBElement<GetStatusResponse> createGetStatusResponse(GetStatusResponse value) {
        return new JAXBElement<GetStatusResponse>(_GetStatusResponse_QNAME, GetStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "createCustomerResponse")
    public JAXBElement<CreateCustomerResponse> createCreateCustomerResponse(CreateCustomerResponse value) {
        return new JAXBElement<CreateCustomerResponse>(_CreateCustomerResponse_QNAME, CreateCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "createCustomer")
    public JAXBElement<CreateCustomer> createCreateCustomer(CreateCustomer value) {
        return new JAXBElement<CreateCustomer>(_CreateCustomer_QNAME, CreateCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateInsuranceAgreementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "createInsuranceAgreementResponse")
    public JAXBElement<CreateInsuranceAgreementResponse> createCreateInsuranceAgreementResponse(CreateInsuranceAgreementResponse value) {
        return new JAXBElement<CreateInsuranceAgreementResponse>(_CreateInsuranceAgreementResponse_QNAME, CreateInsuranceAgreementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendLetter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "sendLetter")
    public JAXBElement<SendLetter> createSendLetter(SendLetter value) {
        return new JAXBElement<SendLetter>(_SendLetter_QNAME, SendLetter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "setStatusResponse")
    public JAXBElement<SetStatusResponse> createSetStatusResponse(SetStatusResponse value) {
        return new JAXBElement<SetStatusResponse>(_SetStatusResponse_QNAME, SetStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "setStatus")
    public JAXBElement<SetStatus> createSetStatus(SetStatus value) {
        return new JAXBElement<SetStatus>(_SetStatus_QNAME, SetStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fsys/", name = "getStatus")
    public JAXBElement<GetStatus> createGetStatus(GetStatus value) {
        return new JAXBElement<GetStatus>(_GetStatus_QNAME, GetStatus.class, null, value);
    }

}
