// This are the SOAP functions for Fsys. 

package fsys;

import javax.jws.WebService; 

@WebService(endpointInterface = "fsys.FsysServer")
public class FsysServerImpl implements FsysServer {
	
	FsysDB fsysDB = new FsysDB();
	
	@Override
	public Integer createCustomer(String SSN, String firstName, String lastName) {	
		Integer customerId = fsysDB.addCustomer(SSN, firstName, lastName);
		System.out.println("SSN=" + SSN + " FirstName=" + firstName + " lastName=" + lastName + " CID=" + customerId);
		return customerId;
	}
	
	@Override
	public Integer createInsuranceAgreement(Integer customerId) {
		Integer agreementId = fsysDB.addInsuranceContract(customerId); 
		System.out.println("CID=" + customerId + " agreementId=" + agreementId);
		return agreementId;
	}
	
	@Override
	public String setStatus(Integer transactionId, String status) {
		fsysDB.setStatus(transactionId, status);
		System.out.println("transactionId=" + transactionId + " status=" + status);
		return status;
	}
	
	@Override
	public String getStatus(Integer transactionId) {
		return fsysDB.getStatus(transactionId); 
	}
	
	@Override
	public Integer sendLetter(Integer customerId, Integer agreementId) {
		// Send letter to customer 
		return 1; 
	}
	
}
