package intl;

import java.io.IOException;
import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;

public class IntlRESTStartUp {
	public static void main(String[] args) throws IllegalArgumentException, IOException {
		HttpServer server = HttpServerFactory.create("http://localhost:8010/intl");
		server.start();
		System.out.println("Press Enter to stop the server. ");
		System.in.read();
		server.stop(0);
	}
}
