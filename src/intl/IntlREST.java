// This is the INTL REST server. It interfaces FSYS SOAP-API and returns JSON. 

package intl;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;

import fsys.FsysServer;
import fsys.FsysServerImplService;

@Path("/rest")
public class IntlREST {

	@GET
	@Path("CreateCustomerInsurance/{transactionId}/{SSN}/{firstName}/{lastName}")
	@Produces(MediaType.TEXT_PLAIN)
	public String CreateCustomerInsurance(@PathParam("transactionId") Integer transactionId,
										  @PathParam("SSN") String SSN,  
										  @PathParam("firstName") String firstName,
										  @PathParam("lastName") String lastName) {
		Integer customerId = 0;  
		Integer agreementId = 0;
		String  status;    
		
		try {
			FsysServerImplService fsysService = new FsysServerImplService();
		    FsysServer fsys = fsysService.getFsysServerImplPort();
		    
			customerId  = fsys.createCustomer(SSN, firstName, lastName);
			agreementId = fsys.createInsuranceAgreement(customerId);
			if(fsys.sendLetter(customerId, agreementId) == 1) 
				status  = fsys.setStatus(transactionId, "avtale sendt");
			else
				status = fsys.setStatus(transactionId,  "Avtale ikke sendt");
		} 
		catch (Exception e) {
		    status = e.toString();
			System.out.println("Error: " + e); 
		}
			
        JSONObject json = new JSONObject ();
		json.put("customerID", customerId);
		json.put("agreementID", agreementId);
		json.put("status", status);

	    return json.toJSONString();
	}  
}