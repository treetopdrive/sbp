// This is the end-user client. It calls the INTL REST API and receives 
// JSON-messages which are just printed out. 

package intlclient;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import org.json.simple.JSONObject; 
import org.json.simple.parser.*; 

 
public class IntlClient {

	public static JSONObject submitTransaction(String arg) 
			throws URISyntaxException, ParseException {
		URI uri = new URI("http", null, "localhost", 8010, arg, null, null);

		System.out.println(uri);
		Client c = Client.create();
    	WebResource resource = c.resource(uri.toString());
    	String response = (String) resource.get(String.class);
    	System.out.println("Response: " + response);
    	
    	JSONParser parser = new JSONParser();
		Object obj = parser.parse(response);
		JSONObject jsonObject = (JSONObject) obj;
    	
    	return jsonObject; 
	}
	
	public static void CreateCustomerInsurance(int transactionId, String SSN, String firstName, String lastName) 
			throws URISyntaxException, ParseException {
		JSONObject response = submitTransaction("/intl/rest/CreateCustomerInsurance/" + transactionId + "/" + SSN + "/" + firstName + "/" + lastName);  
		
		System.out.println("Kunde-nummer  " + response.get("customerID"));
		System.out.println("Avtale-nummer " + response.get("agreementID"));
		System.out.println("Status        " + response.get("status"));
	}
	
    public static void main(String[] args) 
    		throws UnsupportedEncodingException, MalformedURLException, URISyntaxException, ParseException {
    	CreateCustomerInsurance(10, "82345678901", "Anne Panne", "Kanne");
    	CreateCustomerInsurance(11, "12345678901", "Marit", "Parit");
    	CreateCustomerInsurance(12, "45678932232", "Hildur", "Pildur");
    }
     	
}

